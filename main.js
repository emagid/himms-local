$(document).ready(function(){


    // DISABLE RIGHT CLICKS
    document.addEventListener('contextmenu', event => event.preventDefault());


    // CLICK ACTION
    var timer;
    $(".click_action").on({
         'click': function clickAction() {
             var self = this;
                $(self).css('transform', 'scale(.8)');
              timer = setTimeout(function () {
                  $(self).css('transform', 'scale(1)');
              }, 100);
         }
    });


    // ROUTING
    $(document).on('click', '.click', function(){        
        var self = this;
        $(self).css('transform', 'scale(.95)');
        setTimeout(function(){
            $(self).css('transform', 'scale(1)');
            activeCover();
        }, 200);
        changePage($(self).attr('data-page'));
    });

    $(document).on('click', '.home', function(){
        $('.cover').css('background-color', '#29abca');
        $('.jQKeyboardContainer').hide();

        timer = setTimeout(function(){
            activeCover();
            $('.content').fadeOut(500);
            timer = setTimeout(function(){
                window.location = 'index.html';                           
            }, 2000);
        }, 100);
    });

    $(document).on('click', '.back', function(){
        changePage('projects');
    });

    function changePage(page) {
        $('.cover').css('background-color', '#29abca');
        $('.jQKeyboardContainer').hide();

        timer = setTimeout(function(){
            activeCover();
            $('.content').fadeOut(500);
            timer = setTimeout(function(){
                window.location = page+'.html';                           
            }, 2000);
        }, 100);
    }

    $(document).on('click', '.proj', function(){
        kioskPage(this);
    });

    function kioskPage(el) {
        timer;
        page = $(el).attr('data-page');
        color = $(el).css('background-color');
        element = el;
        $('.cover').css('background-color', color);
        activeCover();
        $('.content').fadeOut(1000);
        timer = setTimeout(function(){
            $('.kiosk_show').show();
            $('.kiosk_show .proj_img').attr('src', $(el).attr('data-kiosk'));  
            if ( $(el).attr('data-table') == "true" ) {
                $('.kiosk_holder').addClass('table');
            }else {
                $('.kiosk_holder').removeClass('table');
            }   
        }, 1000);

        
        // $.ajax({
        //     url:'/home/'+page,
        //     type:'GET',
        //     success: function(data){
        //       $('.jQKeyboardContainer').hide();
        //         timer = setTimeout(function(){
        //             activeCover();
        //             $('.content').fadeOut(500);
        //             timer = setTimeout(function(){
        //                 $('.content').html($(data).find('.content').html()).fadeIn();  
        //                 $('iframe').attr('src', $(el).attr('data-kiosk'));  
        //                 $('.lrg').html($(el).attr('data-name')); 
        //                 if ( $(el).attr('data-table') == "true" ) {
        //                     $('.kiosk_holder').addClass('table');
        //                 }else {
        //                     $('.kiosk_holder').removeClass('table');
        //                 }                   
        //             }, 2000);
        //         }, 100);
        //     }
        // });
    }

    function activeCover() {
        $('.cover').addClass('open');
        setTimeout(function(){
            $('.cover').removeClass('open');
        }, 2000);
    }



    // TIMEOUT
    var initial = null;

        function invoke() {
            initial = window.setTimeout(
                function() {
                    $('.white').fadeIn();
                    setTimeout(function(){
                        window.location.href = '/';
                    }, 500);
                }, 6000000000);
        }


        invoke();

        $('body').on('click mousemove', function(){
            window.clearTimeout(initial);
            invoke();
            // invoke2();
        })



});



