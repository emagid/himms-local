$(document).ready(function(){

    // =============================  TAKE PHOTO  ==================================
    $(document).on('click', '#take_pic, .retake, .share', function(){        
        var self = this;
        $(self).css('transform', 'scale(.95)');
        setTimeout(function(){
            $(self).css('transform', 'scale(1)');
        }, 200);
    });

    function countDown() {
      $('.color_slide').fadeOut(500);
        var divs = $('.countdown');
        var timer
        var offset = 0 

        divs.each(function(){
            var self = this;
            timer = setTimeout(function(){
                $(self).css('font-size', '150px');
                $(self).fadeOut(1000);
            }, 1000 + offset);
            offset += 1000;
        });

        // flash
        setTimeout(function(){
          $('.flash').addClass('active');
            $('.flash').fadeIn(1000);
            setTimeout(function(){
              $('.flash').fadeOut(500);
            }, 2000);
            setTimeout(function(){
                $('.step_one').hide()
                $('.step_two').show()
              }, 1000);
            $(divs).css('font-size', '0px');
        }, 4000);
    }

    function convertCanvasToImage(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        console.log(image)
        return image;
    }

    function showPictures() {
        $('#pictures').fadeIn(2000);
        var photos = $('#pictures').children('.photo');
            $(photos[0]).fadeIn();

        $.post('/contact/save_img/',{image:$($(photos[0]).html()).attr('src')},function(data){
            $('#submit_form input[name=image]').val(data.image.id);
        });
        createFabric();
    }




    function createFabric() {
      let canvas = new Canvas('fabric');
      canvasObj = new fabric.Canvas('fabric');
      // $('.upper-canvas, .lower-canvas').css('width', '1000px');
      // $('.upper-canvas, .lower-canvas').css('height', '750px');

      $('#pictures').prepend($('.canvas-container'));
    }

    function Canvas(id) {
        this.canvas = document.createElement('canvas');
        this.canvas.id = id;
        document.body.appendChild(this.canvas);
        return this.canvas;
    }

    $(document).on('click', '.props img', function(){
      fabric.Image.fromURL($(this).attr('src'), function(oImg) {
        oImg.scale(.5).set('flipX', false);
        canvasObj.add(oImg);
      });

      // var imgInstance = new fabric.Image($(this)[0], {
      //   left: 50,
      //   top: 50
      // });
      // imgInstance.scaleToWidth(50);
      // canvasObj.add(imgInstance);

    });


    // PHOTO CLICK
    $(document).on('click', '#take_pic',function(){
        $('#take_pic').css('pointer-events', 'none');
        $('#take_pic p').fadeOut(500);
        var pics = []

        countDown();
            setTimeout(function(){
                $('#pictures').prepend("<div class='canvas_holder photo'><canvas id='pic' class='canvas' width='1000' height='750'></canvas></div>")
                
                var canvas = $('#pic');
                var context = canvas[canvas.length -1].getContext('2d');
                var video = document.getElementById('video');
                var background = $('#background')[0];
                context.drawImage(video, 0, 0, 1000, 750);               

                $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))

            }, 4000);

        setTimeout(function(){
            $('.submit, .retake').delay(1000).fadeIn();
            $('.print').delay(1000).fadeIn();
            $(this).css('pointer-events', 'all');
            showPictures();
        }, 5000);
    });



    // =============================  SHARING PHOTO  ==================================
    $(document).on('click', '.retake', function(){
      setTimeout(function(){
        window.location = 'selfie.html';
      }, 500);
    });

    $(document).on('click', '.share', function(){
        // $.post('/contact/save_img/',{image:$('#pictures > img:last-child').attr('src')},function(data){
        //     $('#submit_form input[name=image]').val(data.image.id);
        // });
        $('.step_two').slideUp(500);
        setTimeout(function(){
          $('.step_three').slideDown(500);
        }, 500);

        $('.fa-close').fadeIn();
     });

    $(document).on('click', '.input', function(){
      $('.step_three').css('margin-top', '500px');
    });

    $(document).on('click', '.fa-close',function(){
      $('.step_three').css('margin-top', '100px');
        $('.jQKeyboardContainer').slideUp();
        $('.step_three').slideUp(500);
        setTimeout(function(){
          $('.step_two').slideDown(500);
        }, 500);
        $('.fa-close').fadeOut();
    });


     $(document).on('click', '#share_btn', function(){
          $('.fa-close').fadeOut();
          $('.jQKeyboardContainer').fadeOut();

          $('#share_alert').slideDown();
          $('#share_alert').css('display', 'flex');
          $('#submit_form').submit();
          $.post('/contact', $('#submit_form').serialize(), function (response) {
              if(response.status){
                  $('#submit_form')[0].reset();
                  window.location.href = '/';
              }else {
                window.location.href = '/';
              }
          });

          setTimeout(function(){
            window.location.href = '/';
          }, 5000);
     });

     $('#submit_form').submit(function(e){
      e.preventDefault();
     });





    // =============================  FILTERS  ==================================
      $('.filter_btn').click(function(){
        $('.filters_holder').css('width', '900px');
        $('.off_click').fadeIn(300);
        setTimeout(function(){
          $('.filters_holder div').fadeIn(300);
        }, 500);
        $('.filters_holder div');
      });

      $('.off_click, .fa-close').click(function(){
        $('.filters_holder div').fadeOut();
        $('.filters_holder').css('width', '0');
        $('.off_click').fadeOut(300);
        setTimeout(function(){
          $('.filters_holder .fa-close').show();
        }, 1000)
      });

      $('.filters_holder h2').click(function(){
        $('.filters_holder h2').removeClass('filter_active');
        $(this).addClass('filter_active');
      });

        $('#add_email').click(function(){
            $('.first_email').parent().append("<input required class='input jQKeyboard' name='email[]' pattern='[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$' type='text' placeholder='Email' title='Please enter a valid email address.'>");
            $('.first_email').parent().find('input').last().initKeypad({'keyboardLayout': keyboard});
        });

        $('#add_phone').click(function(){
            $('.first_phone').parent().append("<input required class='input jQKeyboard' name='phone[]' placeholder='Phone Number (eg: +19874563210)'>");
            $('.first_phone').parent().find('input').last().initKeypad({'keyboardLayout': keyboard});
        });

        $('#video').fadeIn(1000);

        setTimeout(function(){
            $('.pic_text').fadeIn(2000);
            $('#take_pic').fadeIn(2000);
            $('#snap_gif').fadeIn(2000);
        }, 1000);

        setTimeout(function(){
            $('#video').fadeIn(1000);
        }, 2000);



       // Gif Next click
       function picAction( pic ) {
        $(pic).fadeIn('fast');
        $(pic).delay(1300).fadeOut(400);
      }

       function gif( pics ) {
        var offset = 0

        for ( i=0; i<2; i++ ) {
          pics.each(function(){
            var timer
            var self = this;
            timer = setTimeout(function(){
                picAction(self)
            }, 0 + offset);

            offset += 1500;
          });
        }
      }


       $('.print').click(function(){
            saveDrawing();  
            var source = $($('#pictures img:last-child')[1]).attr('src');
            VoucherPrint(source);
       });


       function convertCanvasToImage(canvas) {
          var image = new Image();
          image.src = canvas.toDataURL("image/png");
          console.log(image)
          return image;
      }
 
});